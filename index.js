const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const compression = require("compression");
const path = require("path");
const utils = require("./server/utils");

const PORT = process.env.PORT || 3000;

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(compression());

app.use("/api/mail/send", (req, res) => {
  const { from, text } = req.body;
  utils
    .sendMail(`message from: ${from}`, `content: ${text}`)
    .then(result => {
      if (result) return res.status(200).json({ error: null });
      return res.status(500).json({ error: "Could not send mail" });
    })
    .catch(e => res.status(500).json({ error: "Could not send mail" }));
});

app.use("*", (_, res) => {
  res.sendFile(path.join(__dirname, "/build/index.html"));
});

app.listen(PORT, () => console.log(`Server listening on port ${PORT}!`));
