const nodemailer = require("nodemailer");
const config = require("./config");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: config.MAIL.USER,
    pass: config.MAIL.PASS
  }
});

/**
 *
 * @param {string} subject
 * @param {string} text
 */
const sendMail = (subject, text) => {
  const mailOptions = {
    from: config.MAIL.USER,
    to: config.MAIL.USER,
    subject,
    text
  };
  return new Promise(resolve => {
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        resolve(false);
      } else {
        resolve(true);
      }
    });
  });
};

module.exports = {
  sendMail
};
