import React, { Component } from "react";
import classNames from "classnames";
import { connect } from "react-redux";
import { setActiveProcessBox } from "../redux/actions/AppActions";
import Observer from "@researchgate/react-intersection-observer";
import "./styles/ProcessProgressComponent.css";

const ProgressBox = ({
  title,
  description,
  isActive,
  index,
  onPress,
  classProp
}) => {
  const boxStyle = classNames({
    ProgressBox: true,
    active: isActive
  });
  return (
    <section onClick={onPress} className={`${boxStyle} ${classProp}`}>
      <div className="ProgressBox__number">{index}</div>
      <div className="ProgressBox__circle-box">
        <div className="ProgressBox__circle circle-1" />
        <div className="ProgressBox__circle circle-2" />
        <div className="ProgressBox__circle circle-3" />
        <div className="ProgressBox__circle circle-4" />
        <div className="ProgressBox__circle circle-5" />
        <div className="ProgressBox__circle circle-6" />
      </div>
      <div className="ProgressBox__title">{title}</div>
      <div className="ProgressBox__description">{description}</div>
    </section>
  );
};

class ProcessProgressComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: ""
    };
  }
  onBoxPress = i => {
    this.props.setActiveProcessBox(i);
  };
  handleChange = event => {
    this.setState({
      classes: event.isIntersecting ? "ProcessProgressComponent__animation" : ""
    });
  };
  render() {
    const { boxes, activeIndex } = this.props;
    return (
      <Observer onChange={this.handleChange} rootMargin="0% 0% -25%">
        <section className="ProcessProgress__wrapper">
          <h3 className="ProcessProgress__wrapper-title">
            SEE WHAT OUR WORK PROCESS LOOKS LIKE
          </h3>
          <div className="ProcessProgress">
            {boxes.map((box, i) => (
              <ProgressBox
                classProp={this.state.classes}
                key={i}
                {...box}
                isActive={activeIndex === i}
                onPress={() => this.onBoxPress(i)}
              />
            ))}
            <div className="ProcessProgress__bar">
              <div className="ProcessProgress__bar-outer">
                {window.innerWidth > 700 ? (
                  <div
                    className="ProcessProgress__bar-inner"
                    style={{
                      width: `${(activeIndex / (boxes.length - 1)) * 100}%`
                    }}
                  />
                ) : (
                  <div
                    className="ProcessProgress__bar-inner"
                    style={{
                      height: `${(activeIndex / (boxes.length - 1)) * 100}%`
                    }}
                  />
                )}
              </div>
            </div>
          </div>
        </section>
      </Observer>
    );
  }
}

const mapStateToProps = state => ({
  boxes: state.app.progressBoxes,
  activeIndex: state.app.activeBoxIndex
});

const mapDispatchToProps = {
  setActiveProcessBox
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProcessProgressComponent);
