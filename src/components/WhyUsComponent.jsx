import React from "react";
import Icon from "./Icon";
import { connect } from "react-redux";
import Observer from "@researchgate/react-intersection-observer";
import "./styles/WhyUsComponent.css";

const WhyUsBox = ({ item, classProp }) => {
  return (
    <div className={`WhyUs_Body_Box ${classProp}`}>
      <div className="WhyUs_Body_Box_Icon">
        <Icon
          name={item.icon}
          size={item.size}
          primary={item.primary}
          secondary={item.secondary}
        />
      </div>
      <h3 className="WhyUs_Body_Box_Title">{item.title}</h3>
      <p className="WhyUs_Body_Box_Description">{item.description}</p>
    </div>
  );
};
class WhyUs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: ""
    };
  }
  handleChange = event => {
    this.setState({ classes: event.isIntersecting ? "WhyUs__animation" : "" });
  };
  render() {
    const { reasons } = this.props;
    return (
      <Observer onChange={this.handleChange.bind(this)} rootMargin="0% 0% -45%">
        <div className="WhyUs">
          <div className="WhyUs_Header">
            <div className="Header_Circle Circle1">
              <div className="Header_Circle Circle2">
                <div className="Header_Circle Circle3">
                  <div className="Header_Circle Circle4">
                    <div className="Header_Circle_Icon">
                      <Icon
                        name="badge"
                        size="65"
                        primary="#00C0A3"
                        secondary="#fff"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <h1 className="Header_Title">Reasons to work with us</h1>
            <div className="Header_Line" />
          </div>
          <div className="WhyUs_Body">
            {reasons.map((item, i) => {
              return (
                <WhyUsBox classProp={this.state.classes} key={i} item={item} />
              );
            })}
          </div>
        </div>
      </Observer>
    );
  }
}

const mapStateToProps = state => ({
  reasons: state.app.whyUsReasons
});

export default connect(mapStateToProps)(WhyUs);
