import React from "react";
import { toast } from "react-toastify";
import config from "../config";
import "./styles/ContactUs.css";

class ContactUs extends React.Component {
  state = {
    text: "",
    contact: ""
  };
  _onchange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  _onSend = e => {
    e.preventDefault();
    if (this.state.text.length && this.state.contact.length) {
      fetch(`${config.BASE_URL}/mail/send`, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify({
          from: this.state.contact,
          text: this.state.text
        })
      })
        .then(response => {
          if (response && response.ok) {
            toast("Message sent!");
            this.setState({
              text: "",
              contact: ""
            });
          } else {
            toast("Could not send message!");
          }
        })
        .catch(e => {
          toast("Could not send message!");
        });
    }
  };
  render() {
    return (
      <div className="ContactUs">
        <div className="ContactUs_box">
          <h1 className="ContactUs__Title">Get Started Now!</h1>
          <p className="ContactUs__Description">
            Send us your request and we’ll get back to you right away on how to
            get moving.
          </p>
          <div className="ContactUs__Input_box">
            <input
              placeholder="Your Email"
              id="Email"
              className="ContactUs__Email_Input ContactUs__Input ContactUs__Placeholder"
              type="email"
              name="contact"
              value={this.state.contact}
              onChange={this._onchange}
            />
            <textarea
              rows="10"
              className="ContactUs__Message_Input ContactUs__Input"
              placeholder="Your Message"
              name="text"
              value={this.state.text}
              onChange={this._onchange}
            />
            <button onClick={this._onSend} className="ContactUs__Button">
              SEND MESSAGE
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ContactUs;
