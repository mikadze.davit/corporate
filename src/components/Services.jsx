import React from "react";
import Icon from "./Icon";
import Observer from "@researchgate/react-intersection-observer";
import "./styles/ServicesStyles.css";

const BoxItem = ({ icon, title, list = [] }) => {
  return (
    <div className="Services__BoxItem">
      <div className="Services__BoxItem-icon">
        <Icon name={icon} size={96} primary="#00896F" secondary="#00C0A3" />
      </div>
      <div className="Services__BoxItem-title">{title}</div>
      {list[0] && (
        <div className="Services__BoxItem-list">
          <ul>{list.map((item, i) => <li key={i}>{item}</li>)}</ul>
        </div>
      )}
    </div>
  );
};

class Services extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: "Services__box"
    };
  }
  handleChange = event => {
    this.setState({
      classes: event.isIntersecting
        ? "Services__animation Services__box"
        : "Services__box"
    });
  };
  render() {
    return (
      <Observer onChange={this.handleChange} rootMargin="0% 0% -45%">
        <section className="Services">
          <div className="Services__title">
            <h2>We meet client needs every day. What can we do for you ?</h2>
          </div>
          <div className="Services__boxes">
            <div className={this.state.classes}>
              <BoxItem
                icon="laptop"
                title="Web Development"
                list={["Node.js", "React.js", "Progressive Web Apps"]}
              />
            </div>
            <div className={this.state.classes}>
              <BoxItem
                icon="mobile"
                title="Mobile Development"
                list={["React-Native", "iOS Apps", "Android Apps"]}
              />
            </div>
            <div className={this.state.classes}>
              <BoxItem
                icon="pen"
                title="Product Design"
                list={["Product Review", "App Design", "Web Design"]}
              />
            </div>
            <div className={this.state.classes}>
              <BoxItem
                icon="desktop"
                title="Desktop Development"
                list={["Electron.js", "Cross Platform Desktop Apps"]}
              />
            </div>
          </div>
        </section>
      </Observer>
    );
  }
}

export default Services;
