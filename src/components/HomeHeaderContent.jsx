import React from "react";
import "./styles/HomeHeaderContentStyles.css";

const Content = ({ pressHandler }) => {
  return (
    <section className="HomeHeaderContent">
      <h3>
        We're JavaScript enthusiasts you can trust to{" "}
        <span onClick={pressHandler}>build your next app</span> or{" "}
        <span onClick={pressHandler}>join your team remotely</span>.
      </h3>
    </section>
  );
};

export default Content;
