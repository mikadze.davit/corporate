import React from "react";
import "vivid-icons/dist/vivid-icons";

//available icons https://webkul.github.io/vivid/cheatsheet.html

const Icon = ({
  name = "doc",
  size = 24,
  primary = "#1A055D",
  secondary = "#FF613D"
}) => {
  return (
    <i
      data-vi={name}
      data-vi-size={size}
      data-vi-primary={primary}
      data-vi-accent={secondary}
    />
  );
};

export default Icon;
