import React from "react";
import NodeIcon from "../assets/images/node.svg";
import ReactIcon from "../assets/images/react.svg";
import MongoIcon from "../assets/images/mongo.svg";
import JSIcon from "../assets/images/js.svg";
import ElectronIcon from "../assets/images/electron.svg";
import SequelizeIcon from "../assets/images/sequelize.svg";
import Observer from "@researchgate/react-intersection-observer";
import "./styles/TechnologiesStyles.css";

class Tech extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: ""
    };
  }
  handleChange = event => {
    this.setState({
      classes: event.isIntersecting ? "Technologies_animation" : ""
    });
  };
  render() {
    return (
      <Observer onChange={this.handleChange} rootMargin="0% 0% -25%">
        <section className="Tech">
          <div className="Tech__title">
            <h2>
              We are experts in JS and our services are powered by amazing tools
              :
            </h2>
          </div>
          <div className="Tech__libs">
            <div className="Tech__lib">
              <img className={this.state.classes} src={NodeIcon} alt="Node" />
            </div>
            <div className="Tech__lib">
              <img className={this.state.classes} src={ReactIcon} alt="React" />
            </div>
            <div className="Tech__lib">
              <img className={this.state.classes} src={MongoIcon} alt="Mongo" />
            </div>
            <div className="Tech__lib">
              <img className={this.state.classes} src={JSIcon} alt="JS" />
            </div>
            <div className="Tech__lib">
              <img
                className={this.state.classes}
                src={ElectronIcon}
                alt="Electron"
              />
            </div>
            <div className="Tech__lib">
              <img
                className={this.state.classes}
                src={SequelizeIcon}
                alt="Sequelize"
              />
            </div>
          </div>
        </section>
      </Observer>
    );
  }
}

export default Tech;
