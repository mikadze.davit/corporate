import React from "react";
import "./styles/HeaderStyles.css";

const Header = ({ children, pressHandler }) => {
  return (
    <header className="header">
      <div className="header__menu">
        <div className="header__logo">
          <h1>HackrHub</h1>
        </div>
        <div className="header__links">
          <ul>
            <li onClick={() => pressHandler('home')}>
              <h2>Home</h2>
            </li>
            <li onClick={() => pressHandler('services')}>
              <h2>Services</h2>
            </li>
            <li onClick={() => pressHandler('tech')}>
              <h2>Tech</h2>
            </li>
            <li onClick={() => pressHandler('why')}>
              <h2>Why Us</h2>
            </li>
            <li onClick={() => pressHandler('process')}>
              <h2>Process</h2>
            </li>
            <li onClick={() => pressHandler('contact')}>
              <h2>Contact</h2>
            </li>
          </ul>
        </div>
      </div>
      {children}
    </header>
  );
};

export default Header;
