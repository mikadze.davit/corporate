const config = {
  BASE_URL: process.env.BASE_URL || "https://hackrhub.com/api"
}

export default config