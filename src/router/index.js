import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { Home } from "../containers";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Routes = () => {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route render={() => <Redirect to="/" />} />
        </Switch>
        <ToastContainer progressClassName="toast__progress-bar" />
      </div>
    </Router>
  );
};

export default Routes;
