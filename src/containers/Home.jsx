import React, { Component } from "react";
import {
  Header,
  HomeHeaderContent,
  Services,
  ProcessProgressComponent,
  Technologies,
  WhyUs,
  ContactUs
} from "../components";
import scrollToComponent from "react-scroll-to-component";

class Home extends Component {
  _onPress = title => {
    switch (title) {
      case "services":
        return this.services && scrollToComponent(this.services);
      case "tech":
        return this.tech && scrollToComponent(this.tech);
      case "why":
        return this.why && scrollToComponent(this.why);
      case "process":
        return this.process && scrollToComponent(this.process);
      case "home":
        return this.home && scrollToComponent(this.home);
      case "contact":
        return this.contact && scrollToComponent(this.contact);
      default:
        return this.contact && scrollToComponent(this.contact);
    }
  };
  render() {
    return (
      <section>
        <div
          ref={section => {
            this.home = section;
          }}
        >
          <Header pressHandler={this._onPress}>
            <HomeHeaderContent pressHandler={this._onPress} />
          </Header>
        </div>
        <div
          ref={section => {
            this.services = section;
          }}
        >
          <Services />
        </div>
        <div
          ref={section => {
            this.tech = section;
          }}
        >
          <Technologies />
        </div>
        <div
          ref={section => {
            this.why = section;
          }}
        >
          <WhyUs />
        </div>
        <div
          ref={section => {
            this.process = section;
          }}
        >
          <ProcessProgressComponent />
        </div>
        <div
          ref={section => {
            this.contact = section;
          }}
        >
          <ContactUs />
        </div>
      </section>
    );
  }
}

export default Home;
