import React, { Component } from "react";
import { Provider } from "react-redux";
import Routes from "./router";
import store from "./redux";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Routes />
      </Provider>
    );
  }
}

export default App;
