export const SET_ACTIVE_BOX = "SET_ACTIVE_BOX";

export const setActiveProcessBox = payload => {
  return {
    type: SET_ACTIVE_BOX,
    payload
  };
};
