import { SET_ACTIVE_BOX } from "../actions/AppActions";

const initialState = {
  modal: false,
  progressBoxes: [
    {
      title: "Idea",
      description:
        "Our experts will ask relevant questions to better understand your goals and set milestones.",
      index: "01"
    },
    {
      title: "Team",
      description:
        "We pick a well-balanced team of developers to efficiently address given tasks.",
      index: "02"
    },
    {
      title: "Feedback",
      description:
        "Our team lead communicates with you daily to keep you on track and make necessary adjustments.",
      index: "03"
    },
    {
      title: "Testing",
      description: "We test the final product to make sure it is bullet proof.",
      index: "04"
    }
  ],
  activeBoxIndex: 0,
  whyUsReasons: [
    {
      title: "Painless communication",
      description:
        "All of our developers speak, read, and write English clearly.",
      size: "60",
      icon: "badge",
      primary: "#00C0A3",
      secondary: "#4B4453"
    },
    {
      title: "Top talent",
      description:
        "We handpick developers who have demonstrated expertise in JS stack.",
      size: "60",
      icon: "badge",
      primary: "#00C0A3",
      secondary: "#4B4453"
    },
    {
      title: "Risk free",
      description: "We offer a no-risk trial period. Pay only if satisfied.",
      size: "60",
      icon: "badge",
      primary: "#00C0A3",
      secondary: "#4B4453"
    }
  ]
};

const App = (state = initialState, action) => {
  switch (action.type) {
    case SET_ACTIVE_BOX:
      return {
        ...state,
        activeBoxIndex: action.payload || 0
      };
    default:
      return state;
  }
};

export default App;
